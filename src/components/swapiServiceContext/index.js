import {
    SwapiServiceConsumer,
    SwapiServiceProvider,
} from "./swapiServiceContext";

export {
    SwapiServiceConsumer,
    SwapiServiceProvider,
};
