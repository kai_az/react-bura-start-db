import React from 'react';
import icon from  './r2d2.png';


import './ErrorIndicator.css';

const ErrorIndicator = () => {
    return (
        <div className='error-indicator'>
            <img src={icon} alt="error-icon" width='200px' height='250px'/>
            <span className='boom'>BOOM!</span>
            <span>Something has gone terribly wrong</span>
            <span>(but we already sent droids to fix it)</span>
        </div>
    );
};

export default ErrorIndicator;



