import React from "react";
import './Spinner.css'

const Spinner = () => {
    return (
        <div className="loadingio-spinner-double-ring-8can0ql42yk">
            <div className="ldio-fvuh7mt3s85">
                <div></div>
                <div></div>
                <div>
                    <div></div>
                </div>
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    )
};

export default Spinner;
