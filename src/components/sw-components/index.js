import PersonDetails from './PersonDetails';
import StarshipDetails from './StarshipDetails';
import PlanetDetails from './PlanetDetails';



import {
    PersonList,
    StarshipList,
    PlanetList
} from './ItemLists';



export {
    PersonList,
    StarshipList,
    PlanetList,
    PersonDetails,
    StarshipDetails,
    PlanetDetails
};
