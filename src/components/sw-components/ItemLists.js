import React from "react";
import ItemList from "../ItemList";
import  { withData, withSwapiService }  from "../hoc-helpers";
import withChildFunction from "../hoc-helpers/withChildFunction";
import compose from "../hoc-helpers/compose";



const renderName = ({ name }) => <span>{name}</span>;
const renderModelName = ({ model, name }) => <span>{name} ({model})</span>;


const mapPersonMethodToProps = (swapiService) => {
    return {
        getData: swapiService.getAllPeople
    }
};

const mapPlanetMethodToProps = (swapiService) => {
    return {
        getData: swapiService.getAllPlanets
    }
};

const mapStarshipMethodToProps = (swapiService) => {
    return {
        getData: swapiService.getAllStarships
    }
};

const PersonList = compose(
    withSwapiService(mapPersonMethodToProps),
    withData,
    withChildFunction(renderName)
)(ItemList);

const StarshipList = compose(
    withSwapiService(mapStarshipMethodToProps),
    withData,
    withChildFunction(renderModelName)
)(ItemList);

const PlanetList = compose(
    withSwapiService(mapPlanetMethodToProps),
    withData,
    withChildFunction(renderName)
)(ItemList);

export {
    PersonList,
    StarshipList,
    PlanetList
}
