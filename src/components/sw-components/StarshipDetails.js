import ItemDetails from "../ItemDetails";
import {Record} from "../ItemDetails/ItemDetails";
import React from "react";
import withSwapiService from "../hoc-helpers/withSwapiService";

const StarshipDetails =  (props) => {

    return (
        <ItemDetails {...props} >
            <Record field = 'model' label='Model'/>
            <Record field = 'length' label='Length'/>
            <Record field = 'costInCredits' label='Cost'/>
        </ItemDetails>
    )

};

const mapMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getStarship,
        getImageUrl: swapiService.getStarShipImage
    }
};

export default withSwapiService(mapMethodsToProps)(StarshipDetails);
