import PeoplePage from "./PeoplePage";
import PlanetsPage from "./PlanetsPage";
import StarshipsPage from "./StarshipsPages";
import SecretPage from "./SecretPage";
import LoginPage from "./LoginPage";


export {
    PeoplePage,
    PlanetsPage,
    StarshipsPage,
    SecretPage,
    LoginPage,
};
