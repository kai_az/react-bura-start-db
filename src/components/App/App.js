import React, {Component} from 'react';

import Header from '../Header';
import RandomPlanet from "../RandomPlanet";
import './App.css';
import SwapiService from "../../services/SwapiService";
import {  SwapiServiceProvider } from '../swapiServiceContext';
import ErrorBoundry from "../ErrorBoundry";
import DummySwapiService from "../../services/DummySwapiService";

import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import StarshipDetails from "../sw-components/StarshipDetails";


import {
    PeoplePage,
    PlanetsPage,
    StarshipsPage,
    LoginPage,
    SecretPage
} from '../pages';

export default class App extends Component {



    state = {
        hasError: false,
        swapiService: new SwapiService(),
        isLoggedIn: false
    };

    onLogin = () => {
        this.setState({
            isLoggedIn: true
        })
    };

    onServiceChange = () => {
        this.setState(({swapiService}) => {
            const Service = swapiService instanceof SwapiService ?
                                DummySwapiService : SwapiService;

            return {
                swapiService:  new Service()
            }
        })
    };

    componentDidCatch() {
        this.setState({
            hasError: true
        })
    }


    render() {
        const {isLoggedIn} = this.state;

        return (
            <ErrorBoundry>
                <SwapiServiceProvider value={this.state.swapiService}>
                    <Router>
                        <div className='stardb-app'>

                            <Header onServiceChange={this.onServiceChange}/>
                            <RandomPlanet/>

                            <Switch>
                                <Route
                                    path='/'
                                    render={() => <h2>Welcome to starDB</h2>}
                                    exact
                                />
                                <Route path='/people/:id?' component={PeoplePage}/>
                                <Route path='/planets' component={PlanetsPage}/>
                                <Route path='/starships'   component={StarshipsPage} exact/>
                                <Route path='/starships/:id'
                                       render={({match}) => {
                                           const {id} = match.params;
                                           return <StarshipDetails itemId = {id}/>
                                       }}
                                />
                                <Route
                                    path='/login'
                                    render = {() => {
                                        return <LoginPage
                                            isLoggedIn={isLoggedIn}
                                            onLogin={this.onLogin}
                                        />
                                    }}
                                />
                                <Route
                                    path='/secret'
                                    render = {() => {
                                        return <SecretPage isLoggedIn={isLoggedIn}/>
                                    }}

                                />
                                <Route render={() => <h2>Page not found!!!</h2>}/>
                            </Switch>

                        </div>
                    </Router>
                </SwapiServiceProvider>
            </ErrorBoundry>
        )
    }
}


